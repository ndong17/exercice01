version: '3.3'
services:
    wordpress:
        image: wordpress:4.8
        restart: always
        ports:
            - 80:80
            
        volumes:
          - www:/var/www/html
    postgresql:
        image: postgresql
        restart: always
        environment:
            POSTGRESQL_PASSWORD: example
        volumes:
          - db:/var/lib/postgresql
volumes:
    www:
    db: 
